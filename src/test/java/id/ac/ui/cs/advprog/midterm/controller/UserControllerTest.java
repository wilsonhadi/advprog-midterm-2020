package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository repository;

    @Mock
    private BindingResult result;

    @Mock
    private Model model;
    private User user;
    private String userId;

    @BeforeEach
    public void setupTest() {
        user = new User();
        user.setId(123456);
        user.setName("Test");
        user.setEmail("test@test.com");
        userId = Long.toString(user.getId());
        when(repository.findById(user.getId()))
                       .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    public void testCorrectTemplateAddUser() throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void testCorrectTemplateSignUp() throws Exception {
        mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void testCorrectTemplateEdit() throws Exception {
        mockMvc.perform(get("/edit/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void whenUpdateURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(post("/update/" + userId)
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void testCorrectTemplateDelete() throws Exception {
        mockMvc.perform(get("/delete/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void testEditInvalidIdShouldRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/edit/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining("Invalid user Id");
        }
    }

    @Test
    public void testDeleteInvalidIdShouldRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/delete/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining("Invalid user Id");
        }
    }

    @Test
    void testUpdateInvalidUserStay() throws Exception {
        user.setName(null);
        mockMvc.perform(post("/update/" +  user.getId())
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    void testAddUserInvalidUserStay() throws Exception {
        user.setName(null);
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }


}
