package id.ac.ui.cs.advprog.midterm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import id.ac.ui.cs.advprog.midterm.controller.UserController;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
class MidtermProgrammingExamApplicationTests {

	@Autowired
	private UserController controller;

	@Test
	void contextLoads() {
		MidtermProgrammingExamApplication.main(new String[]{});
		assertThat(controller).isNotNull();
	}

}
