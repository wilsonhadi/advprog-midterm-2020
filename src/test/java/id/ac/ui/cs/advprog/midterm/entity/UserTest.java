package id.ac.ui.cs.advprog.midterm.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserTest {

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setName("test");
    }

    @Test
    public void testSetIdGetId() {
        long id = 12345;
        user.setId(id);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    public void testSetNameGetName() {
        String name = "test";
        user.setName(name);
        assertThat(user.getName()).isEqualTo(name);
    }

    @Test
    public void testSetEmailGetEmail() {
        String email = "test@test.com";
        user.setEmail(email);
        assertThat(user.getEmail()).isEqualTo(email);
    }
}
